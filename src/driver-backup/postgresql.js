const { createGzip } = require('zlib');
const { pipeline } = require('stream');
const fs = require('fs');
const { spawn } = require("child_process");

exports.backup = (config) => {
    return new Promise(async (resolve, reject) => {
        try {
            const files = new Array();
            const databases = config.postgresql.databases;

            const username = config.postgresql.username;
            const password = config.postgresql.password;

            const host = config.postgresql.host;
            const port = config.postgresql.port;

            const pathBin = config.postgresql.pathBin;
            const pathOut = __dirname + '/../../tmp';

            if (!fs.existsSync(pathOut))
                fs.mkdirSync(pathOut);

            let date = new Date();
            date = date.toISOString();
            date = date.substring(0, date.lastIndexOf('.'));
            date = date.split('-').join('');
            date = date.split(':').join('');
            date = date.split('T').join('');

            for (let i = 0; i < databases.length; i++) {
                let file = pathOut + "/" + databases[i] + "_" + date + ".backup";
                let pgDump = "pg_dump";

                if (pathBin)
                    pgDump = pathBin + "/" + pgDump;

                await new Promise(async (resolvePG, rejectPG) => {
                    let booError = false;
                    
                    const exec = spawn(pgDump, [
                        "--dbname=postgres://" + username + ":" + password + "@" + host + ":" + port + "/" + databases[i],                        
                        "--file=" + file
                    ]);

                    exec.stdout.on("data", data => {
                        console.log(data);
                    });

                    exec.stderr.on("data", data => {
                        booError = true;
                        console.log(data.toString());
                    });

                    exec.on('error', (error) => {
                        booError = true;
                        rejectPG(error);
                    });

                    exec.on("close", code => {
                        if (booError) {
                            rejectPG();
                        } else {
                            const gzip = createGzip();
                            const source = fs.createReadStream(file);
                            const destination = fs.createWriteStream(file + '.gz');

                            pipeline(source, gzip, destination, (err) => {
                                fs.unlinkSync(file);
                                if (err) {
                                    rejectPG(err);
                                } else {
                                    file = file + '.gz';
                                    resolvePG();
                                }
                            });
                        }
                    });
                });

                files.push(file);
            }

            resolve(files);
        } catch (e) {
            reject(e);
        }
    });
};  