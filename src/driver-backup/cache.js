const { createGzip } = require('zlib');
const { pipeline } = require('stream');
const fs = require('fs');
const path = require('path');
const { spawn } = require("child_process");

exports.backup = (config) => {
    return new Promise(async (resolve, reject) => {
        try {
            const pathOut = __dirname + '/../../tmp';

            if (!fs.existsSync(pathOut))
                fs.mkdirSync(pathOut);

            let date = new Date();
            date = date.toISOString();
            date = date.substring(0, date.lastIndexOf('.'));
            date = date.split('-').join('');
            date = date.split(':').join('');
            date = date.split('T').join('');


            await new Promise(async (resolveC, resolveC) => {
                const exec = spawn("net", ["stop", "Cache_c-_cachesys"]);

                exec.stdout.on("data", data => {
                    console.log(data);
                });

                exec.stderr.on("data", data => {
                    console.log(data.toString());
                });

                exec.on('error', (error) => {
                    resolveC(error);
                });

                exec.on("close", code => {
                    resolveC();                                            
                });
            });

            const file = pathOut + "/" + date + ".gz"; 
            const gzip = createGzip();
            const source = fs.createReadStream(config.posgresql.database);
            const destination = fs.createWriteStream(file);

            pipeline(source, gzip, destination, async (err) => {
                if (err) {
                    reject(err);
                } else {
                    await new Promise(async (resolveC, resolveC) => {
                        const exec = spawn("net", ["start", "Cache_c-_cachesys"]);
        
                        exec.stdout.on("data", data => {
                            console.log(data);
                        });
        
                        exec.stderr.on("data", data => {
                            console.log(data.toString());
                        });
        
                        exec.on('error', (error) => {
                            resolveC(error);
                        });
        
                        exec.on("close", code => {
                            resolveC();                                            
                        });
                    });

                    files.push(file);
                    resolve(files);
                }
            });
        } catch (e) {
            reject(e);
        }
    });
};  