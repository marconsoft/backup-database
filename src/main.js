(async () => {
    const fs = require('fs');
    const path = require('path');
    const args = process.argv.slice(2);

    if (args.length == 0) {
        console.error('Arquivo de configuração não informado!');
        return;
    }

    const configPath = args[0];

    if (!fs.existsSync(configPath)) {
        console.log('Arquivo de configuração ', configPath, ' não encontrado!');
        return;
    }

    let config = null;

    try {
        let rawdata = fs.readFileSync(configPath);
        config = JSON.parse(rawdata);
    } catch (e) {
        console.error('Não foi possivel ler o arquivo de configuração!');
        return;
    }

    let driverBackup = null;
    try {
        driverBackup = require(__filename + '/../../src/driver-backup/' + config.type + '.js');
    } catch (e) {
        console.error('Tipo de backup inválido.');
        console.error(e);
        return;
    }

    let driverOut = null;
    try {
        driverOut = require(__filename + '/../../src/driver-out/' + config.out.type + '.js');
    } catch (e) {
        console.error('Tipo de saída invalido');
        console.error(e);
        return;
    }

    let tmpFiles = null;
    try {
        console.log('gerando o backup');
        tmpFiles = await driverBackup.backup(config);
    } catch (e) {
        console.error('Não foi possivel gerar o backup!');
        console.error(e);
    }

    if (tmpFiles) {
        for (let i = 0; i < tmpFiles.length; i++) {
            try {
                console.log('gravando o backup ' + path.basename(tmpFiles[i]));
                await driverOut.write(config, tmpFiles[i]);
            } catch (e) {
                console.error('Não foi possivel escrever o arquivo!');
                console.error(e);
            }

            fs.unlinkSync(tmpFiles[i]);
        }
    }


    try {
        console.log('Removendo backups antigos')
        await driverOut.removeOlds(config);
    } catch (e) {
        console.error('Não foi remover os backups antigos!');
        console.error(e);
    }


    console.log('backup finalizado');
})()