const fs = require('fs')
const path = require('path');

exports.write = (config, file) => {
    if (!fs.existsSync(config.out.path)) {
        fs.mkdirSync(config.out.path);
    }

    const readableStream = fs.createReadStream(file)
    const writableStream = fs.createWriteStream(config.out.path + '/' + path.basename(file));

    return new Promise((resolve, reject) => {
        readableStream.pipe(writableStream)
            .on('error', (err) => {
                reject(err);
            })
            .on('close', () => {
                resolve();
            })
    });
};


exports.removeOlds = (config) => {
    if (config.out.stayDays) {
        return new Promise((resolve, reject) => {
            try {
                const files = fs.readdirSync(config.out.path);

                for (let i = 0; i < files.length; i++) {
                    const stats = fs.statSync(config.out.path + '/' + files[i]);
                    const days = Math.ceil(Math.abs(new Date().getTime() - stats.mtime) / (1000 * 60 * 60 * 24));

                    if (days > config.out.stayDays) {
                        fs.unlinkSync(config.out.path + '/' + files[i]);
                    }
                }

                resolve();
            } catch (e) {
                reject(e);
            }
        })
    }

}; 
