const unirest = require('unirest');
const fs = require('fs');
const mime = require('mime-types');

exports.write = (config, file) => {
  return new Promise((resolve, reject) => {
    try {

      let ext = file.substring(file.lastIndexOf('.') + 1, file.lenght);
      if (ext == 'gz') {
        let lastExt = file.substring(0, file.lastIndexOf('.'));
        lastExt = lastExt.substring(lastExt.lastIndexOf('.') + 1, lastExt.lenght);
        ext = lastExt + '.' + ext;
      }

      unirest('GET', config.out.urlMonitor + '/monitoring/application/generate-url-backup-database?' + 'database=' + config.out.database + '&' + 'ext=' + ext)
        .headers({
          'Authorization': 'Basic ' + generateBase64(config.out.username, config.out.password)
        })
        .end(function (res) {

          if (res.error) {
            reject(res.error);
            return;
          }

          let retorno = JSON.parse(res.raw_body);

          unirest('PUT', retorno.url)
            .headers({
              'Content-Type': mime.lookup(file)
            })
            .send(getByteArray(file))
            .end(function (res) {
              if (res.error) {
                reject(res.error);
                return;
              }
              resolve();
            });

        });

    } catch (e) {
      reject(e);
    }
  })
};


exports.removeOlds = (config) => {
  return new Promise((resolve, reject) => {
    try {

      unirest('DELETE', config.out.urlMonitor + '/monitoring/application/database/' + config.out.database + '/remove-old-backups')
        .headers({
          'Authorization': 'Basic ' + generateBase64(config.out.username, config.out.password)
        })
        .end(function (res) {
          if (res.error) {
            reject(res.error);
            return;
          }
          resolve();
        });

    } catch (e) {
      reject(e);
    }
  })
};

//GERANDO BASE64
function generateBase64(username, password) {
  return btoa(username + ':' + password);
}

//GERANDO BINARIO
function getByteArray(filePath) {
  return fs.readFileSync(filePath);
}