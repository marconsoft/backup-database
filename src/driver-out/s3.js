const fs = require('fs')
const path = require('path');
const { Upload } = require("@aws-sdk/lib-storage");
const { S3Client, ListObjectsV2Command, DeleteObjectCommand } = require("@aws-sdk/client-s3");

exports.write = (config, file) => {
    const bucket = config.out.bucket;
    const fileName = path.basename(file);

    return new Upload({
        client: getClient(config),
        params: {
            Bucket: bucket,
            Key: fileName,
            Body: fs.createReadStream(file),
            queueSize: 4,
            partSize: 1024 * 1024 * 5,
            leavePartsOnError: false,
        }
    }).done();
};

exports.removeOlds = (config) => {
    if (config.out.stayDays) {
        return new Promise(async (resolve, reject) => {
            try {
                const client = getClient(config);
                const command = new ListObjectsV2Command({ Bucket: config.out.bucket });
                const response = await client.send(command);
                const files = response.Contents;

                if (files) {
                    for (let i = 0; i < files.length; i++) {
                        const file = files[i];
                        const days = Math.ceil(Math.abs(new Date().getTime() - file.LastModified.getTime()) / (1000 * 60 * 60 * 24));
                        if (days > config.out.stayDays) {
                            console.log("Removendo arquivo ", file.Key, " | Dias ", days);

                            const deleteCommand = new DeleteObjectCommand({
                                Bucket: config.out.bucket,
                                Key: file.Key,
                            });

                            await client.send(deleteCommand);
                        }
                    }
                }

                resolve();
            } catch (e) {
                reject(e);
            }
        })
    }
};

function getClient(config) {
    const accessKeyId = config.out.accessKeyId;
    const secretAccessKey = config.out.secretAccessKey;
    const region = config.out.region;
    const endpoint = config.out.endpoint;

    const clientConfig = {
        credentials: {
            accessKeyId,
            secretAccessKey
        },
        region
    };

    if (endpoint) {
        clientConfig.region = "region";
        clientConfig.forcePathStyle = true;
        clientConfig.endpoint = endpoint;
    }

    return new S3Client(clientConfig);
}